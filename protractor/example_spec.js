describe('test login', function() {

  let ipEmail, ipPassword, btnSubmit;

  beforeEach(function () {
    ipEmail = element(by.id('exampleInputEmail1'));
    ipPassword = element(by.id('exampleInputPassword1'));
    btnSubmit = element(by.id('submitBtn'));
    browser.driver.get('http://localhost:4200/');
  });

  it('should redirect to home page on successful login', function() {
    ipEmail.sendKeys('admin@nagarro.com');
    ipPassword.sendKeys('admin');
    btnSubmit.click().then(function() {
      browser.waitForAngular();
      expect(browser.driver.getCurrentUrl()).toMatch('/home');
    }, 10000);
  });

  it('display alert message in case incorrect credentials are entered', function () {
    ipEmail.sendKeys('admin@nagarro.com');
    ipPassword.sendKeys('dummy');
    btnSubmit.click();
    const alertDialog = browser.switchTo().alert();
    expect(alertDialog.getText()).toEqual("Invalid credentials");
  });

});

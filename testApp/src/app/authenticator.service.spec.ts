/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthenticatorService } from './authenticator.service';

describe('AuthenticatorService', () => {
  let service;
  beforeEach(() => {
    service = AuthenticatorService;
  });

  afterEach(() => {
    service = null;
    localStorage.removeItem("isAuthenticated");
  });

  it('service should exist', () => {
    expect(service).toBeTruthy();
  });

  it('service should authenticate on correct credentials', () => {
    expect(service.checkCredentials("admin@nagarro.com","admin")).toBeTruthy();
  });

  it('service should not authenticate on incorrect credentials', () => {
    expect(service.checkCredentials("dummy","dummy")).toBeFalsy();
  });

  it('service authentication email is case insensitive', () => {
    expect(service.checkCredentials("ADMIN@nagarro.com","admin")).toBeTruthy();
  });

  it('service authentication password is case sensitive', () => {
    expect(service.checkCredentials("admin@nagarro.com","ADMIN")).toBeFalsy();
  });

  it('service isAuthenticated returns true if token is present in local storage ', () => {
    localStorage.setItem("isAuthenticated","true");
    expect(service.isAuthenticated()).toBeTruthy();
  });

  it('service isAuthenticated returns false if token is not present in local storage ', () => {
    expect(service.isAuthenticated()).toBeFalsy();
  });
});

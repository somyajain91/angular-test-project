import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { LoginComponent } from '../login/login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { AuthenticatorService } from '../authenticator.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports :[RouterTestingModule.withRoutes([
        {path:'home', component:DashboardComponent},
        {path:'**', component:LoginComponent}
      ])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    localStorage.removeItem("isAuthenticated");
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should navigate to login page if unauthenticated', () => {
    const navigateSpy = spyOn(router, "navigate");
    const authSpy = spyOn(AuthenticatorService, 'isAuthenticated').and.returnValue(false);
    const component = fixture.componentInstance;
    expect(authSpy).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalled(['/login']);
  });


  it('should render title', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('p').textContent).toContain('dashboard works!');
  });
});

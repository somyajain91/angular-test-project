import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { AuthenticatorService } from '../authenticator.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports :[RouterTestingModule.withRoutes([
        {path:'home', component:DashboardComponent},
        {path:'**', component:LoginComponent}
      ])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to home page on successful login', () => {
      const component = fixture.componentInstance;
      const navigateSpy = spyOn(router, "navigate");
      const authSpy = spyOn(AuthenticatorService, 'checkCredentials').and.returnValue(true);

      component.onSubmitLogin();
      expect(authSpy).toHaveBeenCalledWith("","");
      expect(navigateSpy).toHaveBeenCalledWith(['/home']);
    });
});
